from setuptools import setup, find_packages

setup(
    name = "django-snippets",
    version = "0.0.1",
    url = 'https://mhurt@bitbucket.org/mhurt/django-snippets',
    author = 'Rick Hurst',
    author_email = 'rick.hurst@gmail.com',
    packages = find_packages(),
    zip_safe=True
)
